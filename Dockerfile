FROM node:alpine

ADD package.json .
RUN npm install

ADD index.js .

CMD node index.js
EXPOSE 3000
